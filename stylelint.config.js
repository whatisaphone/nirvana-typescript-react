// https://stylelint.io/user-guide/configure

const nirvana = require('nirvana-stylelint');

const config = nirvana.base();
nirvana.addCSSModules(config);
module.exports = config;
