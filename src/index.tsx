import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom';

import { App } from './ui/app';

main();

function main() {
  const mount = document.getElementById('app');
  ReactDOM.render(
    <StrictMode>
      <App />
    </StrictMode>,
    mount,
  );
}
