import { hot } from 'react-hot-loader/root';

import * as classes from './app.module.scss';

export const App = hot(() => <h1 className={classes.app}>App</h1>);
