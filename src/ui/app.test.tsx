import * as ReactDOM from 'react-dom';

import { App } from './app';

describe('<App />', () => {
  it('renders without errors', () => {
    const div = document.createElement('div');
    expect(() => {
      ReactDOM.render(<App />, div);
    }).not.toThrow();
  });
});
