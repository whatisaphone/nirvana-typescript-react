// https://eslint.org/docs/user-guide/configuring

const nirvana = require('nirvana-eslint');

const config = nirvana.base();
nirvana.addTypeScript(config);
nirvana.addReact(config);
nirvana.addCSSModules(config);
nirvana.addJest(config);
nirvana.merge(config, {
  ignorePatterns: ['/coverage/', '/dist/'],
});

module.exports = config;
