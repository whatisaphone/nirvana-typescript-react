const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

module.exports = function webpackConfig(env) {
  const dev = !(env && env.prod);

  return {
    bail: true,
    mode: dev ? 'development' : 'production',
    entry: [...(dev ? ['react-hot-loader/patch'] : []), './src'],
    resolve: {
      extensions: ['.js', '.ts', '.tsx'],
      alias: {
        ...(dev ? { 'react-dom': '@hot-loader/react-dom' } : undefined),
      },
    },
    module: {
      rules: [
        {
          test: /\.[jt]sx?$/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                envName: dev ? 'development' : 'production',
                plugins: [...(dev ? ['react-hot-loader/babel'] : [])],
              },
            },
          ],
        },
        {
          test: /\.tsx?$/,
          use: {
            loader: 'ts-loader',
            options: {
              transpileOnly: true, // Use `ForkTsCheckerWebpackPlugin` instead
            },
          },
        },
        {
          test: /\.scss$/,
          use: [
            ...(dev ? ['style-loader'] : [MiniCssExtractPlugin.loader]),
            'css-loader',
            'sass-loader',
          ],
        },
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        BUILD_CONFIG: JSON.stringify({ dev }),
      }),
      new ForkTsCheckerWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
      }),
      ...(dev ? [] : [new MiniCssExtractPlugin()]),
    ],
    devServer: {
      hot: true,
    },
  };
};
