# nirvana-typescript-react

An opinionated [TypeScript] + [React] project starter. There are many like it, but this one is mine.

[TypeScript]: https://www.typescriptlang.org/
[React]: https://reactjs.org/

## Development

### Install prerequisites

- [Node.js]
- [Yarn]
- [pre-commit]

[Node.js]: https://nodejs.org/
[Yarn]: https://yarnpkg.com/
[pre-commit]: https://pre-commit.com/

### Install the pre-commit hook

```sh
pre-commit install
```

This installs a Git hook that runs a quick sanity check before every commit.

### Install dependencies

```sh
yarn
```

### Run the app

```sh
yarn dev
```

This starts a server at http://localhost:8080/.

### Run the tests

```sh
yarn test
```
